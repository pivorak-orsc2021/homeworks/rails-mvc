require 'rails_helper'

RSpec.describe(LikesController, type: :routing) do
  describe 'routing' do
    it 'routes to #create' do
      expect(post: '/likes').to(route_to('likes#create'))
    end
  end
end
